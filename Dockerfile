FROM node:lts-alpine AS dev

WORKDIR /app

COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json

RUN npm ci

COPY . /app

RUN sed -i '/"homepage":/d' package.json

ENV CI=true
ENV PORT=8080

CMD [ "npm", "start" ]

FROM dev AS build

RUN npm run build

FROM node:lts-alpine as prod

WORKDIR /app

RUN npm install -g http-server

COPY --from=build /app/build /app

EXPOSE 8080

ENTRYPOINT ["http-server", "."]
